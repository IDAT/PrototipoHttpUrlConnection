package pe.edu.torres.alex.prototipohttpurlconnection.model;

public enum MetodoEnum {
    POST,
    GET,
    PUT,
    DELETE;
}
