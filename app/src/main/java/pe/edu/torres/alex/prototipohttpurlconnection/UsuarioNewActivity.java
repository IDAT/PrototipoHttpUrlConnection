package pe.edu.torres.alex.prototipohttpurlconnection;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import pe.edu.torres.alex.prototipohttpurlconnection.controller.UsuarioController;
import pe.edu.torres.alex.prototipohttpurlconnection.model.MetodoEnum;

public class UsuarioNewActivity extends AppCompatActivity {
    EditText edtCod, edtNom, edtPwd, edtDes, edtEma;
    UsuarioController usuarioController = new UsuarioController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_new);

        // ENLAZAR OBJECT XML A OBJECT JAVA
        edtCod = findViewById(R.id.edtCodigo);
        edtNom = findViewById(R.id.edtNombre);
        edtPwd = findViewById(R.id.edtPasswd);
        edtDes = findViewById(R.id.edtDescri);
        edtEma = findViewById(R.id.edtEmail);
        edtCod.setEnabled(false);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null ) {
            edtCod.setText(bundle.getString("codigo"));
            edtNom.setText(bundle.getString("nombre"));
            edtPwd.setText(bundle.getString("passwd"));
            edtDes.setText(bundle.getString("descri"));
            edtEma.setText(bundle.getString("email"));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_usuario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_eliminar ){
            new Hilo(new JSONObject(),MetodoEnum.DELETE).execute();
            return true;
        }

        if (id == R.id.action_guardar){
            //TODO - CREAR REQUEST JSON
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("usu_codigo", edtCod.getText());
                jsonObject.put("usu_nombre", edtNom.getText());
                jsonObject.put("usu_descri", edtNom.getText());
                jsonObject.put("usu_passwd", edtPwd.getText());
                jsonObject.put("usu_email", edtEma.getText());
                jsonObject.put("usu_imagen", null);
                jsonObject.put("usu_fecreg", "2018-01-02 00:00:00");
                jsonObject.put("usu_estcod", 1);
                //jsonObject.put("usu_estdes", null);
            }catch (JSONException e){
                e.printStackTrace();
            }

            new Hilo(jsonObject,(edtCod.getText().length() > 0 ? MetodoEnum.PUT : MetodoEnum.POST)).execute();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //CLASE ASYNCTASK
    public class Hilo extends AsyncTask<String,Integer,JSONObject> {
        // PROPIEDADES DE CLASES
        JSONObject jsonObjectValores;
        MetodoEnum metodo;

        //DIALOG
        ProgressDialog dialog = null;

        //CONSTRUCTOR
        public Hilo(JSONObject jsonObjectValores, MetodoEnum metodo) {
            this.jsonObjectValores = jsonObjectValores;
            this.metodo = metodo;
        }

        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(UsuarioNewActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Procesando...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            JSONObject jsonObject= null;
            switch (this.metodo){
                case POST:
                    jsonObject = usuarioController.insUsuario(this.jsonObjectValores);
                    break;
                case PUT:
                    jsonObject = usuarioController.updUsuario(this.jsonObjectValores);
                    break;
                case DELETE:
                    jsonObject = usuarioController.delUsuario(edtCod.getText().toString());
                    break;
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                Toast.makeText(getApplicationContext(),jsonObject.getString("message"), Toast.LENGTH_LONG).show();

            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }

            finish();
        }

    }
}
