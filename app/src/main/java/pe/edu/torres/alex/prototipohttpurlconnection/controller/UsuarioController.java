package pe.edu.torres.alex.prototipohttpurlconnection.controller;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.edu.torres.alex.prototipohttpurlconnection.model.MetodoEnum;

public class UsuarioController {

    //final static String strURLRest = "http://10.0.2.2:8080/inventarios/Usuario/Rest";
    final static String strURLRest = "http://192.168.0.100:80/Slim2.6.2";
    //final static String strURLRest = "http://10.0.2.2:3000";
    URL url = null;
    HttpURLConnection httpURLConnection = null;
    OutputStreamWriter outputStreamWriter = null;
    JSONObject jsonObject = null;

    public UsuarioController() {

    }

    public JSONObject httpURLConnectionServer(String strURL, MetodoEnum metodo, JSONObject jsonObject){
        try {
            url = new URL(strURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
            httpURLConnection.setUseCaches(false);
            //httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setConnectTimeout(10000); // 10 Seg
            httpURLConnection.setReadTimeout(15000); // 15 Seg
            switch (metodo){
                case POST:
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);

                    outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream(),"UTF-8");
                    outputStreamWriter.write(jsonObject.toString());
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    break;
                case GET:
                    httpURLConnection.setRequestMethod("GET");
                    break;
                case PUT:
                    httpURLConnection.setRequestMethod("PUT");

                    outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream(),"UTF-8");
                    outputStreamWriter.write(jsonObject.toString());
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    break;
                case DELETE:
                    httpURLConnection.setRequestMethod("DELETE");
                    break;
            }

            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
            BufferedReader in = new BufferedReader(inputStreamReader);
            String strLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((strLine = in.readLine()) != null) {
                stringBuffer.append(strLine);
            }
            Log.i("IDAT",stringBuffer.toString());
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                jsonObject = new JSONObject(stringBuffer.toString());
            }

        }catch (Exception e){
            e.printStackTrace();
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject logUsuario(JSONObject jsonObject){
        return httpURLConnectionServer(strURLRest + "/Login", MetodoEnum.POST, jsonObject);
    }

    public JSONObject insUsuario(JSONObject jsonObject){
        return httpURLConnectionServer(strURLRest + "/Usuario", MetodoEnum.POST, jsonObject);
    }

    public JSONObject selUsuarioAll(){
        return httpURLConnectionServer(strURLRest + "/Usuario", MetodoEnum.GET,new JSONObject());
    }

    public JSONObject updUsuario(JSONObject jsonObject){
        return httpURLConnectionServer(strURLRest + "/Usuario", MetodoEnum.PUT,jsonObject);
    }

    public JSONObject delUsuario(String codigo){
        return httpURLConnectionServer(strURLRest + "/Usuario/" + codigo , MetodoEnum.DELETE, new JSONObject());
    }

}
