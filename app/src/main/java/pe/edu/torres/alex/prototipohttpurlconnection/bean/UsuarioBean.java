package pe.edu.torres.alex.prototipohttpurlconnection.bean;


import pe.edu.torres.alex.prototipohttpurlconnection.model.UsuarioModel;

public class UsuarioBean extends UsuarioModel {
    String usu_estdes;

    public String getUsu_estdes() {
        return usu_estdes;
    }

    public void setUsu_estdes(String usu_estdes) {
        this.usu_estdes = usu_estdes;
    }
}
