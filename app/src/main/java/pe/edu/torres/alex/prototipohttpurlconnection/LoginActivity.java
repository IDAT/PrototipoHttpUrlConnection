package pe.edu.torres.alex.prototipohttpurlconnection;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.SharedMemory;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import pe.edu.torres.alex.prototipohttpurlconnection.adapter.UsuarioAdapter;
import pe.edu.torres.alex.prototipohttpurlconnection.bean.UsuarioBean;
import pe.edu.torres.alex.prototipohttpurlconnection.controller.UsuarioController;

public class LoginActivity
        extends AppCompatActivity
implements TextToSpeech.OnInitListener {
    Button btnLogin;
    TextView edtUserName, edtUserPass;
    CheckBox  chkLoginSave;
    UsuarioController usuarioController = new UsuarioController();
    TextToSpeech tts;
    //TODO - SHAREDPREFERENCES
    SharedPreferences loginPreferences;
    SharedPreferences.Editor  loginPrefsEditor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tts = new TextToSpeech(this,this);
        btnLogin = findViewById(R.id.LogBtnLogin);
        edtUserName = findViewById(R.id.LogEdtUserName);
        edtUserPass = findViewById(R.id.LogEdtUserPass);
        chkLoginSave = findViewById(R.id.LogChbRecord);

        //TODO - SHAREDPREFERENCES
        loginPreferences =getSharedPreferences("loginPrefs",MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        //TODO - EVALUAR & CARGAR PREFERENCES
        Boolean bLogin = loginPreferences.getBoolean("loginSave",false);
        if(bLogin){
            String u = loginPreferences.getString("userName","");
            String p = loginPreferences.getString("userPass","");
            edtUserName.setText(u);
            edtUserPass.setText(p);
            chkLoginSave.setChecked(true);

            //TODO - LLAMAR AL HILO SECUNDARIO
            new HttpRequestTask().execute(edtUserName.getText().toString(),edtUserPass.getText().toString());
        }

        //TODO - BOTON LOGIN EVENTO CLIC
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO - LLAMAR AL HILO SECUNDARIO
                new HttpRequestTask().execute(edtUserName.getText().toString(),edtUserPass.getText().toString());
            }
        });

        Animation mLoadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        mLoadAnimation.setDuration(500);

    }

    @Override
    protected void onDestroy() {
        if(tts != null){
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        if(status ==TextToSpeech.SUCCESS){
            int  res = tts.setLanguage(Locale.getDefault());
            if(res == TextToSpeech.LANG_MISSING_DATA){
                Log.i("TTS","No tenemos soporte");
            }else{
                Log.i("TTS","Tenemos soporte");
            }

        }else{
            Log.e("TTS","Fallo al Iniciar");
        }
    }


    private class HttpRequestTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(LoginActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Validando Usuario...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {

            //TODO - CREAR REQUEST JSON
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("usu_nombre", argumento[0]);
                jsonObject.put("usu_passwd", argumento[1]);
            }catch (JSONException e){
                e.printStackTrace();
            }

            return usuarioController.logUsuario(jsonObject);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
                if(dialog.isShowing())dialog.dismiss();
                //TODO - MOSTRAR KEY MESSAGE
                Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                tts.speak(jsonObject.getString("message"),TextToSpeech.QUEUE_FLUSH,null);
                if(jsonObject.getBoolean("status") && jsonObject.has("status")) {

                    //TODO - GUARDAR SHAREDPREFRENCES
                    if(chkLoginSave.isChecked()) {
                        loginPrefsEditor.putBoolean("loginSave", true);
                        loginPrefsEditor.putString("userName", edtUserName.getText().toString().trim());
                        loginPrefsEditor.putString("userPass", edtUserPass.getText().toString().trim());
                    }else{
                        loginPrefsEditor.clear();
                    }
                    loginPrefsEditor.commit();

                    //TODO - LLAMAR A ACTIVITY
                    Intent intent = new Intent(getApplicationContext(),UsuarioActivity.class);
                    startActivity(intent);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
